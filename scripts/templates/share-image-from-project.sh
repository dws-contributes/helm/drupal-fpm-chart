oc policy add-role-to-group system:image-puller system:serviceaccounts:{{ okd_namespace }} --namespace={{ drupal_image.namespace }}
oc policy add-role-to-group system:image-puller system:serviceaccounts:{{ okd_namespace }} --namespace={{ varnish.namespace }}
oc policy add-role-to-group system:image-puller system:serviceaccounts:{{ okd_namespace }} --namespace={{ shib_apache.namespace }}
oc policy add-role-to-group system:image-puller system:serviceaccounts:{{ okd_namespace }} --namespace={{ redis.namespace }}

