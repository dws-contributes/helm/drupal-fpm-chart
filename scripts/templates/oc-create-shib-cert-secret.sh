echo "creating shibboleth cert secret..."
oc create secret generic shib-certs-{{ okd_namespace }}-{{ okd_application_name }} --from-file=tls.crt={{ cert.common_name }}.crt --from-file=tls.key={{ cert.common_name }}.key
oc patch secret shib-certs-{{ okd_namespace }}-{{ okd_application_name }} -p '{"metadata":{"labels":{"app":"{{ okd_namespace }}-{{ okd_application_name }}"}}}'