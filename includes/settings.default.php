<?php
// @codingStandardsIgnoreFile

$databases['default']['default'] = array (
  'database' => getenv('DB_NAME'), // same as $DB_NAME
  'username' => getenv('DB_USER'), // same as $DB_USER
  'password' => getenv('DB_PASSWORD'), // same as $DB_PASSWORD
  'host' => getenv('DB_HOST'), // same as $DB_HOST
  'driver' => getenv('DB_DRIVER'),  // same as $DB_DRIVER
  'port' => getenv('DB_PORT'), // different for PostgreSQL
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql', // different for PostgreSQL
  'prefix' => '',
);

$settings['deployment_identifier'] = \Drupal::VERSION;

$config_directories = [];

$settings['config_sync_directory'] = (getenv('CONFIG_SYNC_DIRECTORY')) ? getenv('CONFIG_SYNC_DIRECTORY') : '../config/sync';

$settings['hash_salt'] = getenv('HASH_SALT');

$settings['update_free_access'] = FALSE;

/**
 * Specify every reverse proxy IP address in your environment.
 * This setting is required if $settings['reverse_proxy'] is TRUE.
 */

if (getenv('REVERSE_PROXY') && (getenv('REVERSE_PROXY') == 'true')) {
  $settings['reverse_proxy'] = TRUE;
  $proxy = $_SERVER['REMOTE_ADDR'];
  $settings['reverse_proxy_addresses'] = [$proxy];
  /*
 * Symfony 4 doesn't let us inspect custom headers, so we have to create the traditional ones.
 * https://symfony.com/doc/current/deployment/proxies.html
 */
  $_SERVER['X_FORWARDED_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
  $_SERVER['X_FORWARDED_PORT'] = $_SERVER['HTTP_X_FORWARDED_PORT'];
  $_SERVER['X_FORWARDED_PROTO'] = $_SERVER['HTTP_X_FORWARDED_PROTO'];
  $_SERVER['X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
  $_SERVER['FORWARDED'] = $_SERVER['HTTP_FORWARDED'];
  $settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_ALL | \Symfony\Component\HttpFoundation\Request::HEADER_FORWARDED;
}

$settings['file_temp_path'] = '/tmp';

$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';

if (getenv('TRUSTED_HOST_PATTERNS')) {
  $patterns = str_replace(' ', '', getenv('TRUSTED_HOST_PATTERNS'));
  $settings['trusted_host_patterns'] = explode(',', $patterns);
}

/**
 * S3fs settings
 */

$settings['s3fs.access_key'] = getenv('S3FS_ACCESS_KEY');
$settings['s3fs.secret_key'] = getenv('S3FS_SECRET_KEY');
$settings['s3fs.use_s3_for_public'] = (getenv('S3FS_USE_S3_FOR_PUBLIC') == 'true');
$settings['s3fs.upload_as_private'] = (getenv('S3FS_UPLOAD_AS_PRIVATE') == 'true');
$settings['s3fs.use_s3_for_private'] = (getenv('S3FS_USE_S3_FOR_PRIVATE') == 'true');

/**
 * REDIS settings
 */
if (getenv('USE_REDIS') && (getenv('USE_REDIS') == 'true')) {
  $settings['cache']['default'] = 'cache.backend.redis';
  $settings['redis.connection']['interface'] = getenv('REDIS_INTERFACE'); // Can be "Predis".
  $settings['redis.connection']['host'] = getenv('REDIS_HOST');  // Your Redis instance hostname.
  if (getenv('REDIS_PASSWORD')) {
    $settings['redis.connection']['password'] = getenv('REDIS_PASSWORD');
  }
  $settings['redis_compress_level'] = (getenv('REDIS_COMPRESS_LEVEL')) ? getenv('REDIS_COMPRESS_LEVEL') : 1;
  $settings['container_yamls'][] = 'modules/contrib/redis/example.services.yml';
}

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['entity_update_batch_size'] = 50;

// Automatically generated include for settings managed by ddev.
if (file_exists($app_root . '/' . $site_path . '/settings.ddev.php')) {
  include $app_root . '/' . $site_path . '/settings.ddev.php';
}

/**
 * Public file path:
 *
 * A local file system path where public files will be stored. This directory
 * must exist and be writable by Drupal. This directory must be relative to
 * the Drupal installation directory and be accessible over the web.
 */
$settings['file_public_path'] = (getenv('FILE_PUBLIC_PATH')) ? getenv('FILE_PUBLIC_PATH') : 'sites/default/files';

if (getenv('FILE_PRIVATE_PATH')) {
  $settings['file_private_path'] = (getenv('FILE_PRIVATE_PATH'));
}

$settings['entity_update_batch_size'] = (getenv('ENTITY_UPDATE_BATCH_SIZE')) ? getenv('ENTITY_UPDATE_BATCH_SIZE') : 50;

$settings['entity_update_backup'] = TRUE;
