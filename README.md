# how to use

this chart requires helm 3 to be installed locally, along with the openshift cli

```
brew install helm
```

also

``` 
brew install openshift-cli
```

then log into openshift through the command line

make sure your oc client is on the correct project.

```
oc project PROJECTNAME
```

cd into the `scripts` directory and run

```
python3 pre-process-namespace.py
```

This will generate a self-signed cert and create the appropriate openshift objects.

Please take note of the secret generated. the name of the secret needs to be in the values.yaml file, similar to `shib_cert_secret: shib-certs-research-test` 

This helm chart expects the drupal php image to be configurable like the https://gitlab.oit.duke.edu/dws/openshift/images/dws-d9-base image.

This helm chart includes Iain Hadgraft's HTTPD/Shibd helm template. 

once the above steps are complete, cd back to the root directory and run:

```
helm install NAME .
```

where NAME is anything you want. Typically, you can just call it bb8, but it can be a project name (lowercase and hyphens only)

# VARNISH is a work in progress

# Redis shoult not be relied upon until a configuration error can be resolved.

# S3 works just fine.
