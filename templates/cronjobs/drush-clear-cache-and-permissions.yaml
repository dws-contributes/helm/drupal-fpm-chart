
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: drush-rm-cache-perms-{{ .Values.okd_application_name }}
spec:
  schedule: "0 7 * * *"
  successfulJobsHistoryLimit: 1
  failedJobsHistoryLimit: 1
  suspend: {{ .Values.test_cronjobs.clear_cache_and_permissions.suspend }}
  jobTemplate:
    spec:
      backoffLimit: 1
      template:
        metadata:
          name: drush-clear-cache-and-permissions-cron
        spec:
          containers:
            - name: clear-cache-and-permissions-cron
              image:  docker-registry.default.svc:5000/{{ .Values.drupal_image.namespace }}/{{ .Values.drupal_image.name }}:{{ .Values.drupal_image.tag }}
              command:
                - /bin/sh
                - -c
                - |
                  php -d memory_limit=1024M -e /var/www/vendor/bin/drush php-eval 'node_access_rebuild()';
                  php -d memory_limit=1024M -e /var/www/vendor/bin/drush warmer:enqueue jsonapi --run-queue;
                  php -d memory_limit=1024M -e /var/www/vendor/bin/drush cr;
              env:
                - name: DB_HOST
                  valueFrom:
                    secretKeyRef:
                      key: host
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: DB_USER
                  valueFrom:
                    secretKeyRef:
                      key: user 
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: DB_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      key: password
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: DB_NAME
                  valueFrom:
                    secretKeyRef:
                      key: database
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: DB_PORT
                  valueFrom:
                    secretKeyRef:
                      key: database
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: DB_DRIVER
                  valueFrom:
                    secretKeyRef:
                      key: driver
                      name: database-{{ template "drupal-8.namespaceAndProject" . }}
                - name: HASH_SALT
                  value: {{ randAlphaNum 55 }}
                - name: APPLICATION_NAME
                  value: {{ template "drupal-8.namespaceAndProject" . }}
    {{ if .Values.redis.enabled }}
                - name: USE_REDIS
                  value: 'true'
                - name: REDIS_INTERFACE
                  value: 'PhpRedis'
                - name: REDIS_HOST
                  value: {{ .Release.Name }}-redis-master.{{ .Values.okd_namespace }}.svc
                - name: REDIS_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      key: redis-password
                      name: {{ .Release.Name }}-redis
    {{- end }}
    {{- if eq .Values.s3.use true }}
                - name: S3FS_ACCESS_KEY
                  valueFrom:
                    secretKeyRef:
                      key: access_key
                      name: s3credentials-{{ template "drupal-8.namespaceAndProject" . }}
                - name: S3FS_SECRET_KEY
                  valueFrom:
                    secretKeyRef:
                      key: secret_key
                      name: s3credentials-{{ template "drupal-8.namespaceAndProject" . }}
                - name: S3FS_USE_S3_FOR_PUBLIC
                  valueFrom:
                    secretKeyRef:
                      key: use_s3_for_public
                      name: s3credentials-{{ template "drupal-8.namespaceAndProject" . }}
            {{- if eq .Values.s3.upload_as_private true }}
                - name: S3FS_UPLOAD_AS_PRIVATE
                  valueFrom:
                    secretKeyRef:
                      key: upload_as_private
                      name: s3credentials-{{ template "drupal-8.namespaceAndProject" . }}
            {{- end }}
            {{- if eq .Values.s3.use_s3_for_private true }}
                - name: S3FS_USE_S3_FOR_PRIVATE
                  valueFrom:
                    secretKeyRef:
                      key: use_s3_for_private
                      name: s3credentials-{{ template "drupal-8.namespaceAndProject" . }}
            {{- end }}
            {{- end }}
            {{- range $name, $value := .Values.drupal_pod_additional_environment_variables }}
                - name: {{ $name }}
                  value: {{ $value }}
            {{- end }}
              volumeMounts:
                - mountPath: /var/www/web/sites/default/files
                  name: files-{{ template "drupal-8.namespaceAndProject" . }}
                  subPath: sites/default/files
                {{ if .Values.services_file }}
                - mountPath: /var/www/web/sites/default/services.yml
                  name: drupal-services
                  readOnly: true
                  subPath: services.yml
                {{- end }}
                {{ if .Values.settings_file }}
                - mountPath: /var/www/web/sites/default/settings.php
                  name: drupal-settings
                  readOnly: true
                  subPath: settings.php
                {{- end }}
                - name: drupal-tempdir
                  mountPath: /tmp
          volumes:
            - name: files-{{ template "drupal-8.namespaceAndProject" . }}
              persistentVolumeClaim:
                claimName: files-{{ template "drupal-8.namespaceAndProject" . }}
            {{ if .Values.settings_file }}
            - name: drupal-settings
              configMap:
                defaultMode: 420
                name: drupal-settings-{{ template "drupal-8.namespaceAndProject" . }}
            {{- end }}
            {{- if .Values.services_file }}
            - name: drupal-services
              configMap:
                defaultMode: 420
                name: drupal-services-{{ template "drupal-8.namespaceAndProject" . }}
            {{- end }}
            - name: drupal-tempdir
              emptyDir: {}
          restartPolicy: Never