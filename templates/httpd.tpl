{{- define "drupal-8.httpdshibd.settings" -}}
serverName: 'https://{{ .Values.route.url }}:443'
shibEntityId: '{{ .Values.route.entityId }}'
httpdConf:
  proxyTpl: "drupal-8.httpdshibd.httpdconf.proxy"
  requestHeaderTpl: "drupal-8.httpdshibd.httpdconf.requestheader"
{{- end -}}

{{- define "httpdshibd.container.httpd.volumemounts" -}}
{{- include "httpdshibd.container.commonvolumemounts" . }}
- name: config
  mountPath: /etc/httpd/conf.d/zz.conf
  subPath: httpd.conf
- name: config
  mountPath: /var/www/health-check/index.html
  subPath: health-check.html
{{- end -}}

{{- define "drupal-8.httpdshibd.httpdconf.proxy" -}}
<Location /Shibboleth.sso>
RewriteEngine off
</Location>
<Directory /var/www/html>
AllowOverride All
RewriteBase /
</Directory>
ProxyPass /Shibboleth.sso/ !
ProxyPass {{ .settings.httpdHealthCheck.aliasPath }} !
ProxyPass /sites/default/files/ !
<FilesMatch "\.php$">
  SetHandler "proxy:fcgi://drupal-{{ template "drupal-8.namespaceAndProject" . }}.{{ .Values.okd_namespace }}.svc:9000"
  ProxyFCGISetEnvIf "true" SCRIPT_FILENAME "/var/www/web%{REQUEST_URI}"
</FilesMatch>
<Proxy "fcgi://drupal-{{ template "drupal-8.namespaceAndProject" . }}.{{ .Values.okd_namespace }}.svc:9000/" enablereuse=on flushpackets=on max=10>
</Proxy>
DirectoryIndex index.php
ProxyTimeout 360
SecRuleRemoveById 200004
SecRuleRemoveById 200003
SecRuleRemoveById 200002
SecRequestBodyNoFilesLimit 1048576
{{- end -}}

{{- define "drupal-8.httpdshibd.httpdconf.requestheader" -}}
{{- include "httpdshibd.httpdconf.requestheader" . }}
RequestHeader unset MAIL
RequestHeader set MAIL "%{MAIL}e" env=MAIL
RequestHeader unset Shibboleth-IsMemberOf
RequestHeader unset Shibboleth_IsMemberOf
RequestHeader set Shibboleth-IsMemberOf "%{Shibboleth-IsMemberOf}e" env=Shibboleth-IsMemberOf
{{- end -}}

{{- define "drupal-8.httpdshibd.deployment" -}}
{{- $settings := include "drupal-8.httpdshibd.settings" . | fromYaml -}}
metadata:
  labels:
    app: {{ template "drupal-8.namespaceAndProject" . }}
  name: httpd-{{ template "drupal-8.namespaceAndProject" . }}
spec:
  strategy: 
    type: RollingUpdate
  template:
    spec:
      containers:
        -
          {{- include "httpdshibd.render" (list . "drupal-8" "container.httpd") | nindent 10 }}
        -
          {{- include "httpdshibd.render" (list . "drupal-8" "container.shibd") | nindent 10 }}
      volumes:
        {{- include "httpdshibd.deployment.volumes" . | nindent 8 }}
        - name: drupal-files
          persistentVolumeClaim:
            claimName: files-{{ template "drupal-8.namespaceAndProject" . }}
        - name: shib-certs
          secret:
            defaultMode: 420
            items:
              - key: tls.crt
                path: tls.crt
              - key: tls.key
                path: tls.key
            secretName: drupal-{{ template "drupal-8.namespaceAndProject" . }}-shib-certs
        {{- if .Values.shib_apache.additional_httpd_pv.load }}
        - name: {{ .Values.shib_apache.additional_httpd_pv.claimName }}
          persistentVolumeClaim:
            claimName: {{ .Values.shib_apache.additional_httpd_pv.claimName }}
        {{- end }}
        {{- if .Values.shib_apache.additional_httpd_configmap.load }}
        - name: {{ .Values.shib_apache.additional_httpd_configmap.name }}
          configMap:
            name: {{ .Values.shib_apache.additional_httpd_configmap.name }}
            defaultMode: 420
        {{- end }}

{{- end -}}

{{- define "drupal-8.httpdshibd.service" -}}
metadata:
  name: httpd-{{ template "drupal-8.namespaceAndProject" . }}
{{- end -}}

{{- define "drupal-8.httpdshibd.container.httpd" -}}
resources:
  {{- toYaml .Values.httpdResources | nindent 2 }}
image: "docker-registry.default.svc:5000/{{ .Values.shib_apache.namespace }}/{{ .Values.shib_apache.http_image_name }}:{{ .Values.shib_apache.http_image_tag }}"
volumeMounts:
  {{- include "httpdshibd.container.httpd.volumemounts" . | nindent 2 }}
  - name: drupal-files
    mountPath: /var/www/html
    readOnly: true
  {{ if .Values.shib_apache.additional_httpd_configmap.load }}
  - name: {{ .Values.shib_apache.additional_httpd_configmap.name }}
    mountPath: {{ .Values.shib_apache.additional_httpd_configmap.mountPath }}
    subPath: {{ .Values.shib_apache.additional_httpd_configmap.subPath }}
    readOnly: true    
  {{ end }}
  {{- if .Values.shib_apache.additional_httpd_pv.load }}
  - name: {{ .Values.shib_apache.additional_httpd_pv.claimName }}
    mountPath: {{ .Values.shib_apache.additional_httpd_pv.mountPath }}
    readOnly: true
  {{- end }}
{{- end }}

{{- define "drupal-8.httpdshibd.container.shibd" -}}
resources:
  requests:
    cpu: 40m
    memory: 70Mi
image: "docker-registry.default.svc:5000/{{ .Values.shib_apache.namespace }}/{{ .Values.shib_apache.shib_image_name }}:{{ .Values.shib_apache.shib_image_tag }}"
volumeMounts:
  {{- include "httpdshibd.container.shibd.volumemounts" . | nindent 2 }}
  - name: shib-certs
    mountPath: /etc/shibboleth/tls/tls.crt
    subPath: tls.crt
  - name: shib-certs
    mountPath: /etc/shibboleth/tls/tls.key
    subPath: tls.key
{{- end -}}

{{ include "httpdshibd.render" (list . "drupal-8" "configmap" (dict "overrideTpl" "-")) }}
---
{{ include "httpdshibd.render" (list . "drupal-8" "service") }}
---
{{ include "httpdshibd.render" (list . "drupal-8" "deployment") }}
