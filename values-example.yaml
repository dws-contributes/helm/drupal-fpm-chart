# Default values for drupal-8.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# the `project` name in openshift
okd_namespace: namespace
# the version of the app you are running. this should be dev|acceptance|prod
okd_application_name: prod

# drupal image to use. this is a php-fpm image.
# auto deploy? should this instance automatically deploy the latest image? false for production instances
drupal_image: 
  name: bb8-composer-local
  tag: latest
  namespace: dws
  auto_deploy_latest_image: false

# set database credentials OR leave `host` empty for local OKD hosted database
database:
  host: db-hostname.oit.duke.edu
  database: db_name
  username: db_user
  password: password
  driver: mysql
  port: 3306

## these values should be just fine. the shib/apache proxy uses the DOUG community supported images.
shib_apache:
  namespace: doug
  http_image_name: duke-modshib
  http_image_tag: centos-apache
  shib_image_name: duke-shibboleth
  shib_image_tag: centos7

# this will optionally create a bitnami service. this utilizes the bitnami/redis helm chart. see `Chart.yaml` to see this as a dependency.
redis: 
  enabled: false
  securityContext:
    enabled: false
  containerSecurityContext:
    enabled: false
  cluster:
    enabled: false
  master: 
    extraFlags: 
      - "--maxmemory-policy allkeys-lru"
      - "--maxmemory 500mb"
  slave:
    extraFlags:
      - "--maxmemory-policy allkeys-lru"
      - "--maxmemory 500mb" 

# at this moment, this does not work as expected.
varnish:
  useVarnish: false
  namespace: dws
  name: varnish

# route: the url is where drupal will live. this route will be proxied through the shib/apache proxy
# entityId: this is for the shibboleth authentication registration (authentication.oit.duke.edu)
# trusted host pattern: the drupal trusted host pattern
# annotation: these defaults will enable the acme cert autorenewal, and restrict this route to duke network only
route:
  url: drupal-content.cloud.duke.edu
  entityId: https://drupal-content.cloud.duke.edu
  trusted_host_pattern: ^drupal\-content\.cloud\.duke\.edu$
  annotation:
    kubernetes.io/tls-acme: 'true'
    haproxy.router.openshift.io/ip_whitelist: 152.3.0.0/16 152.16.0.0/16 67.159.64.0/18 10.0.0.0/8 172.16.0.0/12

# which secret containing the self-signed certs should we load into the shibboleth/apache containers? These were likely generated prior to helm-installing. the `scripts` directory contains a script to generate this secret.
shib_cert_secret: shib-certs-research-test

# files - storage: general drupal uploads file storage size
# db storage - this only applies IF we have a database hosted in openshift
# varnish_storage: storage for the varnish service
files:
  storage_size: 1Gi
  database_storage: 1Gi
  varnish_storage: 1Gi

# typically, letting openshift provide defaults is fine
# however there are instances were you need tighter control over a pod's resources.
# it is advisable to only set a `request`, and a low one at that (cpu: 10m, memory: 64mi)
drupalResources: {}
# limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 10m
  #   memory: 128Mi

# does this drupal instance need a special `services.yml` file?
# base64 encoded services file
services_file: ''
# does this drupal instance need a special `settings.php` file?
# base64 encoded settings file
settings_file: ''

# these are environment variables exposed to the drupal pod
# this is how we set the config sync directory, which can in fact change per site
drupal_pod_additional_environment_variables:
  - CONFIG_SYNC_DIRECTORY: ../config/sync/default
  - FILE_PRIVATE_PATH: ../path/to/private/directory
  - FILE_PUBLIC_PATH: ../path/to/public/files

# the s3 service credentials, which can be generated at https://idms-portunus.oit.duke.edu/portunus/
# if this is to be used, it is typical to enable s3 for public, and false for the other 2
s3:
  use: false
  bucket: ''
  access_key:
  secret_key: 
  use_s3_for_public: true
  upload_as_private: false
  use_s3_for_private: false

# these details are used for the self-signed cert generated for the shibboleth service
cert:
  org: Duke University  # Usually Duke University or Duke Health Technology Solutions
  orgunit: OIT  # The Department managing this shibboleth protected application
  email: oit-dws@duke.edu  # should be an email used to contact a person or group associated with the ORGUNIT
  common_name: drupal-content.cloud.duke.edu  #  The primary domain for the application
  alt_names: []  # Alternate names for the cert
  certificate: '' # the actual certificate
  key: '' # the public key of the cert

# how many drupal-pods should we start with? 1 or 2 is fine. these are pretty efficient pods.
replicaCount: 1

# the rest of the chart details shouldn't be worried about as they don't have a direct bearing on how the services run.
image:
  pullPolicy: IfNotPresent

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: false
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name:

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: drupal-content.cloud.duke.edu
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}
